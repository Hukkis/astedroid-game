﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    public void loadSceneByIndex(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void exitGame()
    {
        Application.Quit();
    }

}
