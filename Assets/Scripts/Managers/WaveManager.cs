﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class WaveManager : MonoBehaviour
{
    [SerializeField] private AssetLabelReference _levelsLabel;
    [SerializeField] private bool _loopLevels = false;
    [SerializeField] private float _timeBetweenLevels = 5.0f;
    private WaitForSeconds _transition;

    private void Start() => Addressables.LoadAssets<Level>(_levelsLabel.labelString, null).Completed += (op) => StartCoroutine(SetupLevels(op.Result.OrderBy(f => f.Order).ToList()));

    private IEnumerator SetupLevels(IList<Level> levels)
    {
        foreach (var level in levels)
            yield return level.Setup();

        StartCoroutine(RunLevels(levels));
    }

    private IEnumerator RunLevels(IList<Level> levels)
    {
        int idx = 0;
        _transition = new WaitForSeconds(_timeBetweenLevels);
        while (idx < levels.Count)
        {
            yield return levels[idx].RunLevel();
            ++idx;

            if (_loopLevels && idx == levels.Count)
                idx = 0;

            Debug.Log("Changing level");
            yield return _transition;
        }

        Debug.Log("Levels ended");
    }
}
