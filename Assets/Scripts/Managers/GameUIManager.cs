﻿using UnityEngine;
using UnityEngine.UI;

public class GameUIManager : MonoBehaviour
{
    public Text scoreText;

    private static GameUIManager _instance;
    private int currentScore = 0;

    public static GameUIManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameUIManager>();
            return _instance;
        }
    }

    public void UpdateScoreText(int score)
    {
        currentScore += score;
        scoreText.text = "Score: " + currentScore;
    }

}
