﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance = null;

    private AudioSource _audioSource;
    private AudioClip _currentMusic;
    private IList<AudioClip> _playList;

    #region global audioclips

    public AssetReference startingClip;
    public AssetLabelReference playListLabel;

    #endregion

    private void Start()
    {
        if (Instance == null)
            Instance = this;

        _audioSource = GetComponent<AudioSource>();
        Addressables.LoadAssets<AudioClip>(playListLabel.labelString, null).Completed += (op) =>
        {
            _playList = new List<AudioClip>(op.Result);
            StartCoroutine(PlayMusic());
        };
    }

    private IEnumerator PlayMusic()
    {
        var operation = startingClip.LoadAsset<AudioClip>();
        yield return operation;

        _currentMusic = operation.Result;
        _audioSource.PlayOneShot(_currentMusic, 1f);

        yield return new WaitForSeconds(_currentMusic.length);

        while (true)
        {
            if(!_audioSource.isPlaying)
            {
                int i = Random.Range(0, _playList.Count);
                _currentMusic = _playList[i];

                _audioSource.clip = _currentMusic;
                _audioSource.PlayOneShot(_currentMusic, 1f);

                yield return new WaitForSeconds(_currentMusic.length);
            }

            yield return null;
        }
    }
}
