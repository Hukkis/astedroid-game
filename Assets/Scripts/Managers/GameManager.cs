﻿using UnityEngine;
using UnityEngine.AddressableAssets;

public class GameManager : MonoBehaviour
{
    public int starCount = 100;

    [SerializeField] private AssetReferenceGameObject _starReference;
    [SerializeField] private AssetReferenceGameObject _playerReference;

    private float _depth;

	private void Start ()
    {        
        _playerReference.Instantiate();
        _depth = -Camera.main.transform.position.z;

        int width = Screen.width;
        int height = Screen.height;

        var parent = new GameObject("StarPool");
        for (int i = 0; i < starCount; ++i)
        {
            _starReference.Instantiate(
                Camera.main.ScreenToWorldPoint(
                    new Vector3(
                        Random.Range(0, width),
                        Random.Range(0, height),
                        _depth)),
                Quaternion.identity,
                parent.transform
            );
        }
    }
}
