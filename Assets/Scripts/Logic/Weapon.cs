﻿using UnityEngine;
using UnityEngine.AddressableAssets;
/// <summary>
/// Provides the implementation for firing projectiles
/// </summary>
[CreateAssetMenu(fileName = "data", menuName = "Weapon/Default", order = 1)]
public class Weapon : ScriptableObject
{
    [SerializeField] private AssetReferenceGameObject _projectileReference;
    public float fireRate;

    public virtual void Fire(Vector3 position)
    {
        _projectileReference.Instantiate(position, Quaternion.identity);
    }
}
