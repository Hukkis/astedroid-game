﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(fileName = "level", menuName = "Wave/Level", order = 2)]
public class Level : ScriptableObject
{
    [SerializeField] private AssetLabelReference _wavesLabel;
    [SerializeField] private int _loopCount;

    private int _loops;
    private IList<Wave> _waves;

    public string Order => _wavesLabel.labelString;

    public IEnumerator Setup()
    {
        var operation = Addressables.LoadAssets<Wave>(_wavesLabel.labelString, null);
        yield return operation;
        _waves = operation.Result.OrderBy(f => f.Order).ToList();

        foreach(var wave in _waves)
            yield return wave.Setup();
    }

    public virtual IEnumerator RunLevel()
    {
        int idx = 0;
        while(idx < _waves.Count)
        {
            yield return _waves[idx].RunWave();
            ++idx;

            if(idx == _waves.Count && _loops < _loopCount)
            {
                idx = 0;
                ++_loops;
            }
        }
    }
}
