﻿using UnityEngine;
using UnityEngine.AddressableAssets;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float _speed = 10f;
    private float _yBoundary;
    private int _layer;

	private void Start ()
    {
        _layer = LayerMask.GetMask("Enemy");
        _yBoundary = Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y;
    }

	private void Update ()
    {
        var direction = transform.up * _speed * Time.deltaTime;
        var hit = Physics2D.Raycast(transform.position, direction, direction.magnitude, _layer);

        if (hit.collider != null)
        {
            var enemy = hit.transform.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.TakeDamage();
                Addressables.ReleaseInstance(gameObject);
            }
        }

        OutOfBounds();
        transform.Translate(direction);
	}

    private void OutOfBounds()
    {
        if (transform.position.y >= _yBoundary)
            Addressables.ReleaseInstance(gameObject);

        // maybe add width also
    }
}
