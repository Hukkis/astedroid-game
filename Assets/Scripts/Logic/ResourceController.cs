﻿using System.Collections;
using UnityEngine;

public class ResourceController : MonoBehaviour
{
    public float maxRate = 100f;
    public float minRate = 0f;

    [SerializeField, Range(0, 1)] private float _degeneration = 0.5f;
    [SerializeField, Range(0, 1)] private float _regeneration = 0.33f;
    [SerializeField, Range(0, 1)] private float _threshold = 0.2f;
    [SerializeField] private float _regenTime = 1f;
    private float _regenTimer;

    public float Count { get; private set; }
    public bool IsInUse { get; private set; }
    public bool Usable => Count > maxRate * _threshold;

    public void Use() => IsInUse = IsInUse ? true : Usable;

    public bool Use(float count)
    {
        if (Usable)
        {
            Count -= count;
            _regenTimer = Time.time + _regenTime;
            return true;
        }

        return false;
    }

    public void Reset() => Count = maxRate;
    private void Start() => StartCoroutine(Run());

    public void Stop()
    {
        IsInUse = false;
        _regenTimer = Time.time + _regenTime;
    }

    private IEnumerator Run()
    {
        var endOfFrame = new WaitForEndOfFrame();

        while (true)
        {
            if (IsInUse)
            {
                Count -= maxRate * _degeneration * Time.deltaTime;
                if (Count <= minRate)
                    Stop();
            }
            else if (_regenTimer < Time.time)
            {
                Count += maxRate * _regeneration * Time.deltaTime;
            }

            Count = Mathf.Clamp(Count, minRate, maxRate);
            yield return endOfFrame;
        }
    }

    private void OnGUI()
    {
        GUI.VerticalSlider(new Rect(Screen.width - 38, (Screen.height / 2) - 64, 32, 128), Count, minRate, maxRate);
    }
}
