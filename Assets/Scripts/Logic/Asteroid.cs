﻿using UnityEngine;

public class Asteroid : Enemy
{
    [SerializeField] private float _minSpeed = 1.0f;
    [SerializeField] private float _maxSpeed = 4.0f;
    private float _currentSpeed;
    private Vector3 _velocity;

    private void Start()
    {
        Setup();

        _currentSpeed = Random.Range(_minSpeed, _maxSpeed);
        var destination = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width), 0, -Camera.main.transform.position.z));
        _velocity = (destination - transform.position).normalized * _currentSpeed;
    }

    private void Update()
    {
        if (destroyed)
            return;

        direction = _velocity * Time.deltaTime;
        spriteRenderer.transform.Rotate(0, 0, _currentSpeed);

        transform.Translate(direction);
        HitDetection();
    }
}
