﻿using UnityEngine;

public class Droid : Enemy
{
    [SerializeField] private float _speed = 2.5f;
    private float _angle = 90;

    private void Start() => Setup();

    private void Update()
    {
        if (destroyed)
            return;

        direction = new Vector3(Mathf.Cos(_angle), -1, 0) * Time.deltaTime * _speed;
        transform.Translate(direction);
        _angle += Time.deltaTime * _speed;

        HitDetection();
    }
}
