﻿using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;

[RequireComponent(typeof(AudioSource))]
public class PlayerController : MonoBehaviour
{
    #region variables

    [Header("Spacecraft variables")]
    [SerializeField] private AssetReference defaultWeapon;

    [SerializeField] private int _maxLives = 3;
    [SerializeField] private float _invinsibleTime = 5f;
    [SerializeField] private float _sensitivity = 0.01f;
    [SerializeField] private float _horizontalSpeed = 64f;
    [SerializeField] private float _verticalSpeed = 16f;
    [SerializeField] private float _maxSpeed = 16f;
    [SerializeField] private float _speedSmoothTime = 0.1f;
    
    [Header("Roll variables")]
    [SerializeField] private float _rollLength = 0.15f;
    [SerializeField] private float _rollCost = 30f;
    [SerializeField] private float _rollSpeed = 8.0f;
    [SerializeField, Range(0, 1)] private float _sweepPercentage = 0.1f;

    public bool useTouch = false;
    public int CurrentLives { get; private set; }
    public bool Invinsible { get; private set; }
    public bool DisableSpacecraft { get; private set; } = true;
    public Weapon Weapon { get; private set; }
    public Vector2 DefaultPosition { get; private set; }

    private Vector2 _speedSmoothVelocity;
    private float _sweepThreshold;

    private Matrix4x4 _calibrationMatrix;
    private Vector3 _deadZone = Vector3.zero;

    private Vector2? _touchStart;
    private Vector2 _touchVector;
    private Vector2 _velocity;

    // used for clamping the player to the playzone
    private Vector2 _horizontalEdges;
    private Vector2 _verticalEdges;

    private ResourceController _resource;
    private Transform _projectileSpawn;
    private AudioSource _audioSource;
    private Coroutine _firingCoroutine;
    private SpriteRenderer _renderer;
    private WaitForSeconds _waitForFireRate;
    private WaitForSeconds _waitForBarrelRoll;
    private readonly WaitForEndOfFrame _endOfFrame = new WaitForEndOfFrame();

    #endregion

    private void Start()
    {
        Input.gyro.enabled = true;

        _sweepThreshold = Screen.width * _sweepPercentage;
        _waitForBarrelRoll = new WaitForSeconds(_rollLength);

        _projectileSpawn = transform.Find("BulletSpawn");
        _audioSource = GetComponent<AudioSource>();
        _renderer = GetComponent<SpriteRenderer>();
        _resource = GetComponent<ResourceController>();

        CalculateBounds();

        defaultWeapon.LoadAsset<Weapon>().Completed += (op) =>
        {
            EquipWeapon(op.Result);
            Restart();
        };
    }

    private void CalculateBounds()
    {
        Collider2D collider = GetComponent<Collider2D>();

        float z = -Camera.main.transform.position.z;
        DefaultPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2.0f, collider.bounds.center.y, z));

        // Calculate screen space points to world point vectors
        Vector3 tempVec1 = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, z));
        Vector3 tempVec2 = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height / 2.0f, z));

        Vector3 extents = collider.bounds.extents;

        // Use world points as edges
        _horizontalEdges = new Vector2(tempVec1.x + extents.x, tempVec2.x - extents.x);
        _verticalEdges = new Vector2(tempVec1.y + extents.y, tempVec2.y - extents.y);
    }

    private void Update()
    {
        ControlInput();
        CheckEdges();
    }

    #region SpacecraftControl

    private void ControlInput()
    {
        if (!DisableSpacecraft)
        {
            if (useTouch)
                HandleTouch();

            _velocity = Vector2.SmoothDamp(_velocity, GetAcceleration(), ref _speedSmoothVelocity, _speedSmoothTime);
        }

        transform.Translate(_velocity * Time.deltaTime, Space.World);
    }

    private Vector2 GetAcceleration()
    {
        var velocity = Vector2.zero;
        var accelerator = Accelerometer;

        if(!useTouch)
        {
            if (accelerator.y > _sensitivity || accelerator.y < -_sensitivity)
                velocity.y = accelerator.y * _verticalSpeed;
        }
        
        if (accelerator.x > _sensitivity || accelerator.x < -_sensitivity)
            velocity.x = accelerator.x * _horizontalSpeed;

        return Vector2.ClampMagnitude(velocity, _maxSpeed);
    }

    private void CalibrateAccelerometer()
    {
        _deadZone = Input.acceleration;
        var rotateQuaternion = Quaternion.FromToRotation(new Vector3(0f, 0f, -1f), _deadZone);

        var matrix = Matrix4x4.TRS(Vector3.zero, rotateQuaternion, new Vector3(1f, 1f, 1f));
        _calibrationMatrix = matrix.inverse;
    }

    private Vector3 Accelerometer => _calibrationMatrix.MultiplyVector(Input.acceleration);

    private void HandleTouch()
    {
        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);
            if (_touchStart.HasValue)
            {
                _touchVector = touch.position - _touchStart.Value;
                if (_touchVector.magnitude >= _sweepThreshold)
                {
                    if (_resource.Use(_rollCost))
                        StartCoroutine(DoABarrelRoll(_touchVector.normalized));
                    
                    _touchStart = null;
                }
            }
            else
            {
                _touchStart = touch.position;
                _touchVector = Vector2.zero;
            }
        }
        else
        {
            _touchStart = null;
        }
    }

    private IEnumerator DoABarrelRoll(Vector2 direction)
    {
        DisableSpacecraft = true;

        _velocity.y = _rollSpeed * direction.y > 0 ? 1 : -1;
        yield return _waitForBarrelRoll;
        _velocity.y = 0;

        DisableSpacecraft = false;
    }

    // See if the spacecraft is on the edges.
    private void CheckEdges()
    {
        Vector3 pos = transform.position;

        pos.x = Mathf.Clamp(pos.x, _horizontalEdges.x, _horizontalEdges.y);
        pos.y = Mathf.Clamp(pos.y, _verticalEdges.x, _verticalEdges.y);

        transform.position = pos;
    }

    #endregion

    #region DamageTaking

    public void TakeDamage()
    {
        if (!Invinsible)
        {
            CurrentLives--;
            Invinsible = true;

            if (CurrentLives > 0)
                StartCoroutine(DamageableAfterTime());
            else
            {
                //DisableSpaceCraft = true;
                Restart();
            }
        }
    }

    private IEnumerator DamageableAfterTime()
    {
        Color color = _renderer.color;

        float timer = 0f;
        while (timer < _invinsibleTime)
        {
            timer += Time.deltaTime;
            // do animations etc...

            float changeColorTimer = (Time.time * 1000 % 300) / 300;
            _renderer.color = new Color(changeColorTimer, 1 - changeColorTimer, changeColorTimer, 1);

            yield return _endOfFrame;
        }

        _renderer.color = color;
        Invinsible = false;
    }

    private void Restart()
    {
        CalibrateAccelerometer();
        _resource.Reset();

        CurrentLives = _maxLives;
        transform.position = DefaultPosition;

        Invinsible = false;
        DisableSpacecraft = false;
    }

    #endregion

    #region Firing

    public void EquipWeapon(Weapon newWeapon)
    {
        if (newWeapon != null)
        {
            if (_firingCoroutine != null)
                StopCoroutine(_firingCoroutine);

            if (Weapon != null)
                Addressables.ReleaseAsset(Weapon);

            Weapon = newWeapon;
            _waitForFireRate = new WaitForSeconds(newWeapon.fireRate);

            _firingCoroutine = StartCoroutine(Fire());
        }
    }

    private IEnumerator Fire()
    {
        while (true)
        {
            Weapon?.Fire(_projectileSpawn.position);
            yield return _waitForFireRate;
        }
    }

    #endregion
}
