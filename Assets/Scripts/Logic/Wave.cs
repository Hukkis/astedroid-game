﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement;

[CreateAssetMenu(fileName = "wave", menuName = "Wave/BaseWave", order = 1)]
public class Wave : ScriptableObject
{
    [SerializeField] protected AssetLabelReference _waveEnemiesLabel;
    [SerializeField] protected int _enemiesPerWave;
    [SerializeField] protected float _interval;

    public string Order => _waveEnemiesLabel.labelString;

    private float _depth;
    private WaitForSeconds _waitForInterval;
    private int _delay;
    private IList<IResourceLocation> _resources;

    public IEnumerator Setup()
    {
        _depth = -Camera.main.transform.position.z;
        _waitForInterval = new WaitForSeconds(_interval);

        var operation = Addressables.LoadAssets<IResourceLocation>(_waveEnemiesLabel.labelString, null);
        yield return operation;
        _resources = operation.Result;
    }

    public virtual IEnumerator RunWave()
    {
        for (int i = 0; i < _enemiesPerWave; i++)
        {
            SpawnWaveObject(_resources[Random.Range(0, _resources.Count)]);
            yield return _waitForInterval;
        }
    }

    protected virtual void SpawnWaveObject(IResourceLocation resource)
    {
        var randomPos = new Vector3(Random.Range(0, Screen.width), Screen.height, _depth);
        Addressables.Instantiate<GameObject>(
            resource, 
            Camera.main.ScreenToWorldPoint(randomPos),
            Quaternion.identity
        ).Completed += OnEnemyInstantiated;
    }

    protected void OnEnemyInstantiated(IAsyncOperation<GameObject> op)
    {
        var enemy = op.Result.GetComponent<Enemy>();
        if (enemy != null)
            enemy.OnDestroyed += (score) => GameUIManager.Instance.UpdateScoreText(score);
    }
}
