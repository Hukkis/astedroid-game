﻿using UnityEngine;
using System;
using UnityEngine.AddressableAssets;

/// <summary>
/// Base class for every damageable object for player
/// Damaging the object and player is done in this class
/// 
/// Moving the object is done in sub classes
/// using the direction vector provided by this class
/// </summary>
public class Enemy : MonoBehaviour
{
    [SerializeField] private int _maxLives = 3;
    [SerializeField] private int _scoreOnDeath;

    private int _lives;
    private int _layer;
    private bool _outOfBounds = false;

    private Vector2 _yBoundary;
    private Animator _animator;

    protected SpriteRenderer spriteRenderer;
    protected Vector3 direction;
    protected bool destroyed = false;

    public event Action<int> OnDestroyed;
	
    protected void Setup()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        _animator = GetComponentInChildren<Animator>();

        _lives = _maxLives;
        _layer = LayerMask.GetMask("Player");

        _yBoundary.x = Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y;
        _yBoundary.y = Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y;
    }

    protected void HitDetection()
    {
        var hit = Physics2D.Raycast(transform.position, direction, direction.magnitude, _layer);
        if (hit.collider != null)
            hit.transform.GetComponent<PlayerController>()?.TakeDamage();

        if (transform.position.y <= _yBoundary.y && !_outOfBounds)
        {
            _outOfBounds = true;
            Addressables.ReleaseInstance(gameObject, 1f);
        }
    }

    public void TakeDamage()
    {
        if(destroyed)
            return;

        _lives--;
        if (_lives <= 0)
        {
            _animator.SetTrigger("Die");
            destroyed = true;
            direction = Vector3.zero;

            OnDestroyed?.Invoke(_scoreOnDeath);
            Addressables.ReleaseInstance(gameObject, 1f);
        }
    }
}
