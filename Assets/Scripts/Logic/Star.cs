﻿using UnityEngine;

public class Star : MonoBehaviour
{
    public Sprite[] starTypes;
    public float maxSpeed = 10f;
    public float minSpeed = 1f;

    private float _brightness;
    private float _speed;
    private Vector2 _yBoundary;
    private Sprite _starType; 

    private void Start()
    {
        _brightness = Random.value;
        _speed = Random.Range(minSpeed, maxSpeed);

        _yBoundary.x = Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y;
        _yBoundary.y = Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y;

        _starType = starTypes[Random.Range(0, starTypes.Length)];
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        renderer.sprite = _starType;

        Color color = renderer.color;
        color.a = _brightness;
        renderer.color = color;
    }

    private void Update()
    {
        transform.Translate(Vector2.down * _speed * Time.deltaTime);

        if(transform.position.y <= _yBoundary.y)
        {
            Vector3 pos = transform.position;
            pos.y = _yBoundary.x;
            transform.position = pos;
        }
    }

}
