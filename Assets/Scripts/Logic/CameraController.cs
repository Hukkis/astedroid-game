﻿using UnityEngine;

// Calculate current aspect ratio and scale with the default aspect ratio.

public class CameraController : MonoBehaviour
{
    private float _baseAspect = 1920 / 1080;
    CameraController _mainCamera;

	void Start ()
    {
        // calculate current aspect ratio
        float currentAspect = (float)Screen.width / Screen.height;
        Camera.main.projectionMatrix = Matrix4x4.Scale(new Vector3(currentAspect / _baseAspect, 1.0f, 1.0f)) *
            Camera.main.projectionMatrix;
	}
}
